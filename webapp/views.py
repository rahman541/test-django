from django.shortcuts import render
from django.http import HttpResponse
from webapp.models import Post

def index(request):
	posts = Post.objects.all().order_by('-date')
	return render(request, 'home.html', {'posts': posts})

def test(request):
	return HttpResponse("<h2>Hey there!</h2>")

def postShow(request):
	return render(request, 'post_show.html')
