from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.index, name='home'),
	url(r'^test', views.index, name='test'),
	url(r'^(?P<pk>\d+)$', views.postShow, name='post.show'),
]
