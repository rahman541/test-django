## Test Django web framework

## Setup
```
python manage.py migrate
python manage.py createsuperuser
```

## Usage
```
python manage.py runserver
```